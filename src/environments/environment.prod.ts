export const environment = {
  production: true,

  ApiBaseURL: (<HTMLInputElement>document.getElementById('apiUrl')).value,
  websiteRoot: JSON.parse((<HTMLInputElement>document.getElementById('__ClientContext')).value).websiteRoot,
  imageUrl: 'areas/ng/iMISAngular_Reps/assets/image',
  baseUrl: JSON.parse((<HTMLInputElement>document.getElementById('__ClientContext')).value).baseUrl,
  token: (<HTMLInputElement>document.getElementById('__RequestVerificationToken')).value
};
