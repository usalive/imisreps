import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  apiBaseURL =  environment.ApiBaseURL;
  reps_Api = this.apiBaseURL + 'Reps';
  reps_Territory_API = this.apiBaseURL + 'RepTerritories';
  territories_API = this.apiBaseURL + 'Territories';

  public headers: HttpHeaders;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });
  }

  getAllReps(): Observable<any> {
    return this.http.get(this.reps_Api);
  }

  saveReps(RepName: any): Observable<any> {
    const body = new HttpParams().set('Name', RepName).toString();
    return this.http.post(this.reps_Api, body, { headers: this.headers });
  }


  getAllRepsTerritory(): Observable<any> {
    return this.http.get(this.reps_Territory_API);
  }

  saveRepsTerritory(repsTerritory: any): Observable<any> {
    const body = new HttpParams()
      .set('RepName', repsTerritory.RepName)
      .set('TerritoryName', repsTerritory.TerritoryName)
      .set('RepId', repsTerritory.RepId)
      .set('TerritoryId', repsTerritory.TerritoryId)
      .set('Commission', repsTerritory.Commission);
    const requestBody = body.toString();
    return this.http.post(this.reps_Territory_API, requestBody, { headers: this.headers });
  }

  updateRepsTerritory(repsTerritory: any): Observable<any> {
    const body = new HttpParams()
      .set('RepTerretoryId', repsTerritory.RepTerretoryId)
      .set('RepName', repsTerritory.RepName)
      .set('TerritoryName', repsTerritory.TerritoryName)
      .set('RepId', repsTerritory.RepId)
      .set('TerritoryId', repsTerritory.TerritoryId)
      .set('Commission', repsTerritory.Commission);
    const requestBody = body.toString();
    return this.http.put(this.reps_Territory_API, requestBody, { headers: this.headers });
  }

  deleteRepsTerritory(repId, territoryId): Observable<any> {
    return this.http.delete(this.reps_Territory_API + '/' + repId + '/' + territoryId, { headers: this.headers });
  }


  getAllTerritories(): Observable<any> {
    return this.http.get(this.territories_API);
  }

  saveTerritories(TerritoryName: any): Observable<any> {
    const body = new HttpParams().set('Name', TerritoryName).toString();
    return this.http.post(this.territories_API, body, { headers: this.headers });
  }

}

