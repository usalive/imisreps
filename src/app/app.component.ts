import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { Validators } from '@angular/forms';
import { AppService } from './app.service';
import Swal from 'sweetalert2';
import { environment } from '../environments/environment';
import { PACKAGE_ROOT_URL } from '@angular/core/src/application_tokens';
declare var $: any;


@Component({
  selector: 'asi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  repsForm: FormGroup;
  territoryForm: FormGroup;
  repsTerritoryForm: FormGroup;

  isRepsFormSubmitted = false;
  isTerritoryFormSubmitted = false;
  isRepsTerritoryFormSubmitted = false;

  repsList = [];
  territoryList = [];
  repsTerritoryList = [];
  tempRepsTerritory: any[] = [];

  repsListModel: any = {};
  territoryListModel: any = {};
  repsTerritoryListModel: any = {};

  repId = 0;
  territoryId = 0;
  repsTerritoryId = 0;

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';

  errorMessage = 'Something went wrong !';
  isLoading = true;

  constructor(private appService: AppService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService) { ToastrModule.forRoot(); }

  ngOnInit() {
    try {
      let url = window.location.href;
      if (url.indexOf('#') > -1) {
        url = url.substring(0, url.lastIndexOf('#'));
        // do nothing
      }
      location.replace(url + '#/FrepsTerritory');
    } catch (error) {
      console.log(error);
    }

    this.getToken();
    this.validation();

    this.getAllRep();
    this.getAllTerritory();
    this.getAllRepsTerritory();
  }
  get isValidRepId(){
    return this.repsTerritoryForm.get('RepId').invalid && (this.isRepsTerritoryFormSubmitted || this.repsTerritoryForm.get('RepId').dirty || this.repsTerritoryForm.get('RepId').touched );
  }
  get isValidTerritoryId(){
    return this.repsTerritoryForm.get('TerritoryId').invalid && (this.isRepsTerritoryFormSubmitted || this.repsTerritoryForm.get('TerritoryId').dirty || this.repsTerritoryForm.get('TerritoryId').touched );
  }
  get isValidCommission(){
    return this.repsTerritoryForm.get('Commission').invalid && (this.isRepsTerritoryFormSubmitted || this.repsTerritoryForm.get('Commission').dirty || this.repsTerritoryForm.get('Commission').touched );
  }
  get isValidRepName(){
    return this.repsForm.get('RepName').invalid && (this.isRepsFormSubmitted || this.repsForm.get('RepName').dirty || this.repsForm.get('RepName').touched );
  }
  get isValidTerritoryName(){
    return this.territoryForm.get('TerritoryName').invalid && (this.isTerritoryFormSubmitted || this.territoryForm.get('TerritoryName').dirty || this.territoryForm.get('TerritoryName').touched );
  }
  validation() {
    this.repsFormValidation();
    this.territoryFormValidation();
    this.repsTerritoryFormValidation();
  }

  repsFormValidation() {
    this.repsForm = this.formBuilder.group({
      'RepName': [null, Validators.required]
    });
  }

  territoryFormValidation() {
    this.territoryForm = this.formBuilder.group({
      'TerritoryName': [null, Validators.required]
    });
  }

  repsTerritoryFormValidation() {
    this.repsTerritoryForm = this.formBuilder.group({
      'RepId': [null, Validators.required],
      'TerritoryId': [null, Validators.required],
      'Commission': [null, Validators.required]
    });
  }

  getToken() {
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      console.log(error);
    }
  }

  // List Functions
  getAllRep() {
    try {
      this.appService.getAllReps().subscribe(result => {
        if (result.StatusCode === 1) {
          this.repsList = result.Data;
        } else {
          this.repsList = [];
        }
      }, error => {
        this.isLoading = false;
        console.log(error);
      });
    } catch (error) {
      console.log(error);
    }


  }

  getAllRepsTerritory() {
    try {
      this.appService.getAllRepsTerritory().subscribe(result => {
        if (result.StatusCode === 1) {
          this.isLoading = false;
          if (result.Data.length > 0) {
            this.repsTerritoryList = result.Data;
            this.tempRepsTerritory = [...this.repsTerritoryList];
            this.isLoading = false;
          } else {
            this.repsTerritoryList = [];
            this.isLoading = false;
          }
        } else if (result.StatusCode === 3) {
          this.isLoading = false;
          this.repsTerritoryList = [];
          this.isLoading = false;
        } else {
          this.isLoading = false;
          this.repsTerritoryList = [];
          this.isLoading = false;
        }
      }, error => {
        this.isLoading = false;
        console.log(error);
      });
    } catch (error) {
      console.log(error);
    }

  
  }

  getAllTerritory() {
    try {
      this.appService.getAllTerritories().subscribe(result => {
        if (result.StatusCode === 1) {
          if (result.Data.length > 0) {
            this.territoryList = result.Data;
          }
        } else {
          this.territoryList = [];
        }
      }, error => {
        this.isLoading = false;
        console.log(error);
      });
    } catch (error) {
      console.log(error);
    }
  }

  updateFilter(event) {
    try {
    const val = event.target.value.toLowerCase();
    const num = Number(val);
    if (isNaN(num)) {
      const tmpRepsTerritory1 = this.tempRepsTerritory.filter(function (d) {
        return (d.RepName.toLowerCase().indexOf(val) !== -1 || !val)
          || (d.TerritoryName.toLowerCase().indexOf(val) !== -1 || !val);
      });
      this.repsTerritoryList = tmpRepsTerritory1;
    } else {
      const tmpRepsTerritory2 = this.tempRepsTerritory.filter(function (d) {
        return (d.Commission === num || !num);
      });
      this.repsTerritoryList = tmpRepsTerritory2;
    }
    } catch (error) {
      console.log(error);
    }
  }

  // Save, Update & Delete Functions
  saveReps() {
    try {
      this.isRepsFormSubmitted = true;
      if (this.repsForm.valid) {
        if (this.repId === 0) {
          const repName = this.repsForm.controls['RepName'].value;
          this.appService.saveReps(repName).subscribe(result => {
            if (result.StatusCode === 1) {
              this.resetRepsVariables();
              this.getAllRep();
              document.querySelector('#btnreps').innerHTML = 'Create';
              (<HTMLInputElement>document.getElementById('repsModal')).style.display = 'none';
              this.toastr.success('Saved successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
        } else {
        }
      } else {
        return;
      }
    } catch (error) {
      console.log(error);
    }
  }

  saveRepsTerritory() {
    try {
      this.isRepsTerritoryFormSubmitted = true;
      if (this.repsTerritoryForm.valid) {
        if (this.repsTerritoryId === 0) {
          this.repsTerritoryListModel.RepId = this.repsTerritoryForm.controls['RepId'].value;
          this.repsTerritoryListModel.TerritoryId = this.repsTerritoryForm.controls['TerritoryId'].value;
          this.repsTerritoryListModel.Commission = this.repsTerritoryForm.controls['Commission'].value;
          this.appService.saveRepsTerritory(this.repsTerritoryListModel).subscribe(result => {
            if (result.StatusCode === 1) {
              this.resetRepsTerritoryVariables();
              this.getAllRepsTerritory();
              document.querySelector('#btnRepsTerritory').innerHTML = 'Create';
              this.toastr.success('Saved successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
  
        } else {
  
          this.repsTerritoryListModel.RepTerretoryId = this.repsTerritoryId;
          this.repsTerritoryListModel.RepId = this.repsTerritoryForm.controls['RepId'].value;
          this.repsTerritoryListModel.TerritoryId = this.repsTerritoryForm.controls['TerritoryId'].value;
          this.repsTerritoryListModel.Commission = this.repsTerritoryForm.controls['Commission'].value;
          this.appService.updateRepsTerritory(this.repsTerritoryListModel).subscribe(result => {
            if (result.StatusCode === 1) {
              this.clearRepsTerritory();
              this.getAllRepsTerritory();
              this.toastr.success('Saved successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
  
        }
      } else {
        return;
      }
    } catch (error) {
      console.log(error);
    }
  }

  saveTerritory() {
    try {
      this.isTerritoryFormSubmitted = true;
      if (this.territoryForm.valid) {
        if (this.territoryId === 0) {
          const territoryName = this.territoryForm.controls['TerritoryName'].value;
          this.appService.saveTerritories(territoryName).subscribe(result => {
            if (result.StatusCode === 1) {
              this.resetTerritoryVariables();
              this.getAllTerritory();
              document.querySelector('#btnTerritory').innerHTML = 'Create';
              (<HTMLInputElement>document.getElementById('TerritoryModal')).style.display = 'none';
              this.toastr.success('Saved successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
        } else {
        }
      } else {
        return;
      }
    } catch (error) {
      console.log(error);
    }
  }

  deleteRepsTerritory(objRepsTerritory: any) {
    try {
      Swal({
        title: 'Are you sure you want to delete this sales reps ?',
        text: '',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        reverseButtons: true,
        focusConfirm: false
      }).then((res) => {
        if (res.value) {
          this.appService.deleteRepsTerritory(objRepsTerritory.RepId, objRepsTerritory.TerritoryId).subscribe(result => {
            if (result.StatusCode === 1) {
              document.querySelector('#btnRepsTerritory').innerHTML = 'Create';
              this.resetRepsTerritoryVariables();
              this.getAllRepsTerritory();
              this.toastr.success('Deleted successfully.', 'Success!');
            } else {
              this.toastr.error(result.Message, 'Error');
            }
          }, error => {
            this.isLoading = false;
            this.toastr.error(this.errorMessage, '');
          });
        } else if (res.dismiss === Swal.DismissReason.cancel) {
        }
      });
    } catch (error) {
      console.log(error);
    }
  }



  // cancel button event
  clearRepsTerritory() {
    try {
      document.querySelector('#btnRepsTerritory').innerHTML = 'Create';
      document.getElementById('btnCancelRepsTerritory').style.display = 'none';
      this.resetRepsTerritoryVariables();
    } catch (error) {
      console.log(error);
    }
  }


  editRepsTerritory(repsTerritory: any) {
    try {
      this.repsTerritoryId = repsTerritory.TerritoryId;
    this.repsTerritoryForm.controls['RepId'].setValue(repsTerritory.RepId);
    this.repsTerritoryForm.controls['TerritoryId'].setValue(repsTerritory.TerritoryId);
    this.repsTerritoryForm.controls['Commission'].setValue(repsTerritory.Commission);
    document.querySelector('#btnRepsTerritory').innerHTML = 'Update';
    document.getElementById('btnCancelRepsTerritory').style.display = 'inline-block';
    } catch (error) {
      console.log(error);
    }
  }

  // Reset Variables
  resetRepsVariables() {
    this.repsForm.reset();
    this.repId = 0;
    this.isRepsFormSubmitted = false;
    this.repsListModel = {};
  }

  resetRepsTerritoryVariables() {
    this.repsTerritoryForm.reset();
    this.repsTerritoryId = 0;
    this.isRepsTerritoryFormSubmitted = false;
    this.repsTerritoryListModel = {};
  }

  resetTerritoryVariables() {
    this.territoryForm.reset();
    this.territoryId = 0;
    this.isTerritoryFormSubmitted = false;
    this.territoryListModel = {};
  }


  // Modal Open & Close
  openRepsModal() {
    this.resetRepsVariables();
    (<HTMLInputElement>document.getElementById('repsModal')).style.display = 'block';
  }

  closeRepsModal() {
    this.resetRepsVariables();
    (<HTMLInputElement>document.getElementById('repsModal')).style.display = 'none';
  }

  openTerritoryModal() {
    this.resetTerritoryVariables();
    (<HTMLInputElement>document.getElementById('TerritoryModal')).style.display = 'block';
  }

  closeTerritoryModal() {
    this.resetTerritoryVariables();
    (<HTMLInputElement>document.getElementById('TerritoryModal')).style.display = 'none';
  }
}